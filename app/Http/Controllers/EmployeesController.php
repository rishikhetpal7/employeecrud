<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Email;
use App\Models\Employee;
use App\Models\Phone;
use App\Models\Whatsapp;
use Illuminate\Http\Request;

class EmployeesController extends Controller
{
    //

    public function index()
    {

        $employees = Employee::all();
        return view('employee.index', compact([
            'employees'
        ]));
    }

    public function store(Request $request)
    {
        $arrayW = explode('_', $request->whatsapp_primary[0]);
        $whatsapp_primary = $arrayW[2];
        $arrayP = explode('_', $request->phone_primary[0]);
        $phone_primary = $arrayP[2];
        $arrayE = explode('_', $request->email_primary[0]);
        $email_primary = $arrayE[2];
        // dd($request);


        $employee = Employee::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'gender' => $request->gender
        ]);
        $employee->address()->create([
            'location' => $request->location,
            'address1' => $request->address1,
            'address2' => $request->address2,
            'postal_code' => $request->postal_code,
            'taluka' => $request->taluka,
            'suburb' => $request->suburb,
            'city' => $request->city,
            'district' => $request->district,
            'state' => $request->state,
            'country' => $request->country
        ]);
        // if(count($request->whatsapp) < $whatsapp_primary){
        //     $whatsapp_primary--;
        // }
        $i = 1;
        foreach($request->whatsapp as $number){
            $whatsappData = [
                'whatsapp'=> $number,
            ];
            if($i == $whatsapp_primary){
                $whatsappData['primary'] = 1;
                // dd($whatsappData);
            }

            $employee->whatsappContacts()->create($whatsappData);
            $i++;
        }


        $i = 1;
        foreach($request->phone as $number){
            $phoneData = [
                'phone'=> $number,
                'employee_id' => $employee->id
            ];
            if($i == $phone_primary){
                $phoneData['primary'] = 1;
            }
            Phone::create($phoneData);
            $i++;
        }
        $i = 1;

        foreach($request->email as $mail){
            $emailData = [
                'email'=> $mail,
                'employee_id'=>$employee->id
            ];
            if($i == $email_primary){
                $emailData['primary'] = 1;
            }
            Email::create($emailData);
            $i++;
        }

        return redirect(route('employee.index'));



    }


    public function create(){
        return view('employee.create');
    }


    public function edit(Employee $employee)
    {

        $phones = $employee->phones;
        $address = $employee->address()->first();
        $whatsapp_phones = $employee->whatsappContacts;
        $emails = $employee->emails;

        return view('employee.edit', compact([
            'employee',
            'address',
            'phones',
            'whatsapp_phones',
            'emails'
        ]));
    }

    public function update(Request $request, Employee $employee)
    {
        // dd($request);
        $arrayW = explode('_', $request->whatsapp_primary[0]);
        $whatsapp_primary = $arrayW[2];
        $arrayP = explode('_', $request->phone_primary[0]);
        $phone_primary = $arrayP[2];
        $arrayE = explode('_', $request->email_primary[0]);
        $email_primary = $arrayE[2];




        $employee->address()->update([
            'location' => $request->location,
            'address1' => $request->address1,
            'address2' => $request->address2,
            'postal_code' => $request->postal_code,
            'taluka' => $request->taluka,
            'suburb' => $request->suburb,
            'city' => $request->city,
            'district' => $request->district,
            'state' => $request->state,
            'country' => $request->country
        ]);
        $employee->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'gender' => $request->gender
        ]);
        $employee->whatsappContacts()->forceDelete();
        $employee->emails()->forceDelete();
        $employee->phones()->forceDelete();
        $i = 1;
        foreach($request->whatsapp as $number){
            $whatsappData = [
                'whatsapp'=> $number,
            ];
            if($i == $whatsapp_primary){
                $whatsappData['primary'] = 1;
                // dd($whatsappData);
            }

            $employee->whatsappContacts()->create($whatsappData);
            $i++;
        }


        $i = 1;
        foreach($request->phone as $number){
            $phoneData = [
                'phone'=> $number
            ];
            if($i == $phone_primary){
                $phoneData['primary'] = 1;
            }
            $employee->phones()->create($phoneData);
            $i++;
        }
        $i = 1;

        foreach($request->email as $mail){
            $emailData = [
                'email'=> $mail
            ];
            if($i == $email_primary){
                $emailData['primary'] = 1;
            }
            $employee->emails()->create($emailData);
            $i++;
        }

        return redirect(route('employee.index'));



    }



    public function destroy(Employee $employee){
        $employee->address()->forceDelete();
        $employee->phones()->forceDelete();
        $employee->emails()->forceDelete();
        $employee->whatsappContacts()->forceDelete();
        $employee->delete();
        return redirect(route('employee.index'));
    }

}
