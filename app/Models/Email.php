<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    use HasFactory;
    protected $table = "email";
    protected $fillable = [
        'employee_id',
        'email',
        'primary'
    ];

    public function employee(){
        return $this->belongsTo(Employee::class);
    }

    public function getIsPrimaryAttribute(){
        if($this->primary == 1){
            return "checked";
        }
        return "";
    }
}
