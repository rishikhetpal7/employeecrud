<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    use HasFactory;
    protected $table = 'phone_contacts';
    protected $fillable = [
        'phone',
        'employee_id',
        'primary'
    ];

    public function getIsPrimaryAttribute(){
        if($this->primary == 1){
            return "checked";
        }
        return "";
    }

}
