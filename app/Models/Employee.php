<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'gender'
    ];

    public function address(){
        return $this->hasMany(Address::class, 'employee_id', 'id');
    }

    public function emails(){
        return $this->hasMany(Email::class);
    }

    public function phones(){
        return $this->hasMany(Phone::class);
    }

    public function whatsappContacts(){
        return $this->hasMany(Whatsapp::class);
    }

}
