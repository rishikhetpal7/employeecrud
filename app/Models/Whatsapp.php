<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Whatsapp extends Model
{
    use HasFactory;
    protected $table = "whatsapp_contacts";

    protected $fillable = [
        'whatsapp',
        'primary'
    ];

    public function getIsPrimaryAttribute(){
        if($this->primary == 1){
            return "checked";
        }
        return "";
    }
}
