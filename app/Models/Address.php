<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

    protected $table = "address";
    protected $fillable = [
        'employee_id',
        'address1',
        'address2',
        'location',
        'postal_code',
        'postal_area',
        'taluka',
        'suburb',
        'east_west',
        'city',
        'district',
        'state',
        'country'
    ];
}
