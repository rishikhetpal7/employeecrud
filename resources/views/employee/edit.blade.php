@extends("layouts.app")
@section('content')

<div class="card">
    <div class="card-header">Edit Employee</div>
    <div class="card-body">
        <form action="{{ route('employee.update', $employee->id) }}" method="POST" id="myForm">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="first_name">First Name:</label>
                        <input type="text" name="first_name" id="first_name" value="{{ $employee->first_name }}">
                      </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="last_name">Last Name:</label>
                        <input type="text" name="last_name" id="last_name" value="{{ $employee->last_name }}">
                      </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="gender">Gender:</label>
                        <input type="text" name="gender" id="gender" value="{{ $employee->gender }}">
                      </div>
                </div>
            </div>
            <h4>Address</h4>
            <div class="address-block">

            <div class="row">
            <div class="col-4">
            <label for="address">Address 1:</label>
            <input type="text" placeholder="address 1" name="address1" id="address1" value="{{ ($address->address1) }}">
            </div>
            <div class="col-4">
            <label for="address">Address 2:</label>
            <input type="text" placeholder="address2" name="address2" id="address2" value="{{ $address->address2 }}">
            </div>
            <div class="col-4">
            <label for="address">Location:</label>
            <input type="text" placeholder="location" name="location" id="location" value="{{ $address->location }}">
            </div>
            </div>


            <div class="row">
            <div class="col-4">
            <label for="address">postal code:</label>
            <input type="number" placeholder="postal_code" name="postal_code" id="postal_code" value="{{ $address->postal_code }}">
            </div>
            <div class="col-4">
            <label for="address">Taluka:</label>
            <input type="text" placeholder="taluka" name="taluka" id="taluka" value="{{ $address->taluka }}">
            </div>
            </div>


            <div class="row">
            <div class="col-4">
            <label for="address">Suburb:</label>
            <input type="text" placeholder="suburb" name="suburb" id="suburb" value="{{ $address->suburb }}">
            </div>
            <div class="col-4">
            <label for="address">City:</label>
            <input type="text" placeholder="city" name="city" id="city" value="{{ $address->city }}">
            </div>
            </div>


            <div class="row">
            <div class="col-4">
            <label for="address">District:</label>
            <input type="text" placeholder="district" name="district" id="district" value="{{ $address->district }}">
            </div>
            <div class="col-4">
            <label for="address">State:</label>
            <input type="text" placeholder="state" name="state" id="state" value="{{ $address->state }}">
            </div>
            <div class="col-4">
            <label for="address">Country:</label>
            <input type="text" placeholder="country" name="country" id="country" value="{{ $address->country }}">
            </div>
            </div>

            </div>


            <h4>Contact Details:</h4>
            <div class="row">
            <div class="col-md-4" id="phone-adder">
            <div class="heading d-flex justify-content-between">
            <h6>Contacts</h6>
            <button class="btn btn-outline-primary" class="phone-add" onclick="phoneAdd()" type="button">+</button>
            </div>
            <?php $i = 1 ?>
            @foreach($phones as $phone)
            <div id='phone-field' class='row'><button type='button' class='contact-delete' onclick='valueDelete(this)'><i class='fa fa-trash'></i></button><label for='phones'>Contact:</label><input type='number' name='phone[]' id='phone' value="{{ $phone->phone}}"><label for='is_primary'>primary: </label><input type='radio' name='phone_primary[]'  {{ $phone->is_primary }} value="phone_primary_<?=$i?>"></div>
            <?php $i++ ?>
            @endforeach

            </div>
            <div class="col-md-4" id="whatsapp-contact-adder">
            <div class="heading d-flex justify-content-between">
            <h6>Whatsapp Contacts</h6>
            <button class="btn btn-outline-primary" class="whatsapp-contact-add" onclick="whatsappContactAdd()" type="button">+</button>
            </div>
            <?php $i = 1 ?>
            @foreach($whatsapp_phones as $whatsapp)
            <div id='contact-field' class='row'><button type='button' class='whatsapp-contact-delete' onclick='valueDelete(this)'><i class='fa fa-trash'></i></button><label for='phones'>Whatsapp:</label><input type='number' name='whatsapp[]' id='whatsapp' value="{{ $whatsapp->whatsapp}}"><label for='is_primary'>primary: </label><input type='radio' name='whatsapp_primary[]'  {{ $whatsapp->is_primary }} value="whatsapp_primary_<?=$i?>"></div>
            <?php $i++ ?>
            @endforeach

            </div>

            <div class="col-md-4" id="email-adder">
            <div class="heading d-flex justify-content-between">
            <h6>Email</h6>
            <button class="btn btn-outline-primary" class="email-add" onclick="emailAdd()" type="button">+</button>
            </div>
            <?php $i = 1 ?>
            @foreach($emails as $mail)

            <div id='contact-field' class='row'><button type='button' class='email-delete' onclick='valueDelete(this)'><i class='fa fa-trash'></i></button><label for='phones'>Email:</label><input type='email' name='email[]' id='email' value="{{ $mail->email }}"><label for='is_primary'>primary: </label><input type='radio' name='email_primary[]'  {{ $mail->is_primary }} value="email_primary_<?=$i?>"></div>
            <?php $i++ ?>
            @endforeach


            </div>
            </div>

            <div class="card-footer">
            <button type="submit" class="btn btn-primary" id="btn-submit">Save</button>
            </div>
            </form>

      </div>
    </div>

@endsection

@section('page-level-scripts')

<script>
    i = 1;
    function phoneAdd(){
      $("#phone-adder").append(`<div id='phone-field' class='row'><button type='button' class='contact-delete' onclick='valueDelete(this)'><i class='fa fa-trash'></i></button><label for='phones'>Contact:</label><input type='number' name='phone[]' id='phone'><label for='is_primary'>primary: </label><input type='radio' name='phone_primary[]' value='phone_primary_${i}'></div>`);
      i++;
    }
    j=1
    function whatsappContactAdd(){
      $("#whatsapp-contact-adder").append(`<div id='whatsapp-contact-field' class='row'><button type='button' class='whatsapp-contact-delete' onclick='valueDelete(this)'><i class='fa fa-trash'></i></button><label for='whatsapp'>Whatsapp:</label><input type='number' name='whatsapp[]' id='whatsapp'><label for='is_primary'>primary: </label><input type='radio' name='whatsapp_primary[]' value='whatsapp_primary_${j}'></div>`);
        j++;
    }
    k = 1
    function emailAdd(){
      $("#email-adder").append(`<div id='email-field' class='row'><button type='button' class='email-delete' onclick='valueDelete(this)'><i class='fa fa-trash'></i></button><label for='email'>Email:</label><input type='email' name='email[]' id='email'><label for='is_primary'>primary: </label><input type='radio' name='email_primary[]' value='email_primary_${k}'></div>`);
        k++;
    }

    function valueDelete(value){
    //   console.log($(value));
      $(value).parent().remove();
      if($(value).hasClass("contact-delete")){
          console.log($("input[name='phone_primary[]']"));
          $("input[name='phone_primary[]']").each(function(index){
              i = index + 1;
            $(this).val('phone_primary_' + i);
          });
      }else if($(value).hasClass("whatsapp-contact-delete")){
          console.log($("input[name='whatsapp_primary[]']"));
          $("input[name='whatsapp_primary[]']").each(function(index){
              i = index + 1;
            $(this).val('whatsapp_primary_' + i);
          });
      }else if($(value).hasClass("email-delete")){
          console.log($("input[name='email_primary[]']"));
          $("input[name='email_primary[]']").each(function(index){
              i = index + 1;
            $(this).val('email_primary_' + i);
          });
      }
    }

</script>
@endsection


